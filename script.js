const item = document.querySelector('.item')
const placeholders = document.querySelectorAll('.placeholder')

item.addEventListener('dragstart',dragstart)
item.addEventListener('dragend',dragend)

for(placeholder of placeholders){
    placeholder.addEventListener('dragover',dragover)//над плейсхолдером
    placeholder.addEventListener('dragenter',dragenter)//заходим на территорию
    placeholder.addEventListener('dragleave',dragleave)//перенесли но вышли оттуда
    placeholder.addEventListener('drop',dragdrop)//когда отпустил
}

function dragstart(event){
    event.target.classList.add('hold')
    setTimeout(() => event.target.classList.add('hide'),0)
    console.log('взял')
}

function dragend(event){
    event.target.className = 'item'
    console.log('скинул')
}

function dragover(event){
    event.preventDefault()
    console.log('dragover')
}

function dragenter(event){
    event.target.classList.add('hovered')
    console.log('dragenter')
}

function dragleave(event){
    event.target.classList.remove('hovered')
    console.log('dragleave')
}

function dragdrop(event){
    event.target.append(item)
    event.target.classList.remove('hovered')
    console.log('drop')
}